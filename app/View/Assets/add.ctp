<?php

echo $this->Form->create('Assets'); ?>
<fieldset>
    <legend>Add Assets</legend>
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10"><?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false)); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10"><?php echo $this->Form->input('category', array('class' => 'form-control', 'label' => false)); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Purchase Date</label>
                    <div class="col-sm-10"><?php echo $this->Form->input('purchase_date', array('type' => 'date', 'class' => 'dataDate', 'label' => false)); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10"><?php echo $this->Form->input('price', array('type' => 'number', 'label' => false)); ?></div>
                </div>
                <div class="form-group">
                    <?php echo $this->Form->end('Save'); ?>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<?php echo $this->Html->link('List Assets', array('controller'=>'Assets', 'action'=>'index')); ?>