$(document).ready(function () {
    $('.dataDate').datepicker({
        format: 'dd-mm-yyyy',
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
});