-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.25 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table test_knd.assets
CREATE TABLE IF NOT EXISTS `assets` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `assets_number` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `purchase_date` date DEFAULT NULL,
  `price` double DEFAULT NULL,
  `accumulated_depreciation` double DEFAULT NULL,
  `book_value` double DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `assets_id` (`id`),
  KEY `assets_number` (`assets_number`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table test_knd.assets: ~2 rows (approximately)
DELETE FROM `assets`;
/*!40000 ALTER TABLE `assets` DISABLE KEYS */;
INSERT INTO `assets` (`id`, `assets_number`, `name`, `category`, `purchase_date`, `price`, `accumulated_depreciation`, `book_value`, `created`, `modified`) VALUES
	(1, 'KND-2019-0001', 'Toyota Avanza', 'Vehicle', '2019-01-03', 200000000, 12500000, 187500000, '2019-04-04 02:09:24', '2019-04-04 02:09:24'),
	(2, 'KND-2019-3508', 'Toyota Sienta', 'Vehicle', '2019-01-03', 300000000, 18750000, 281250000, '2019-04-04 02:09:24', '2019-04-04 02:09:24'),
	(3, 'KND-2019-1794', 'Suzuki Ertiga', 'Vehicle', '2018-04-04', 300000000, 75000000, 225000000, '2019-04-04 05:37:29', '2019-04-04 05:37:29');
/*!40000 ALTER TABLE `assets` ENABLE KEYS */;


-- Dumping structure for table test_knd.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET latin1 DEFAULT NULL,
  `assets` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table test_knd.roles: ~2 rows (approximately)
DELETE FROM `roles`;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `name`, `assets`) VALUES
	(1, 'admin', '1111'),
	(2, 'staff', '0100');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;


-- Dumping structure for table test_knd.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `roles_id` int(11) DEFAULT NULL,
  `username` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_id` (`id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `FK_Roles` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Dumping data for table test_knd.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `roles_id`, `username`, `password`, `created`, `modified`) VALUES
	(1, 1, 'admin', '6ba0694410b278c18ce36ac8a0156101481167f3', '2019-04-04 07:45:21', '2019-04-04 07:45:21'),
	(2, 2, 'staff', '6ba0694410b278c18ce36ac8a0156101481167f3', '2019-04-04 07:45:21', '2019-04-04 07:45:21');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
