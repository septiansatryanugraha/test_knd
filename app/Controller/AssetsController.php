<?php

class AssetsController extends AppController {

    public $uses = array('Assets');
    public $helpers = array('Html', 'Form', 'Session');
    public $components = array('Session');

    function index() {
        $conditions = array();

        if (!empty($this->params['url']['q'])) {
            $conditions['or']['name like'] = '%' . $this->params['url']['q'] . '%';
            $conditions['or']['assets_number like'] = '%' . $this->params['url']['q'] . '%';
        }

        $this->paginate = array(
            'limit' => 10,
            'conditions' => $conditions
        );
        $assets = $this->paginate('Assets');
        $this->set('assets', $assets);
    }

    public function manageData() {
        $this->set('Assets', $this->Asset->find('all'));
    }

    function add() {
        if (!empty($this->data)) {
            $assets = $this->data['Assets'];
            $purchaseDate = $assets['purchase_date'];
            $purchaseDate = $purchaseDate['year'] . '-' . $purchaseDate['month'] . '-' . $purchaseDate['day'];
            $price = $assets['price'];

            $months = 0;
            if (strlen($purchaseDate) == 0) {
                $purchaseDate = NULL;
            } else {
                $nowDate = date('Y-m-d');
                $year1 = date('Y', strtotime($purchaseDate));
                $year2 = date('Y', strtotime($nowDate));

                $month1 = date('m', strtotime($purchaseDate));
                $month2 = date('m', strtotime($nowDate));

                $months = (($year2 - $year1) * 12) + ($month2 - $month1);
            }

            $accumulatedDepreciation = $months * $price / 48;
            $bookValue = $price - $accumulatedDepreciation;
            if ($bookValue < 0) $bookValue = 0;

            $dataSave['Assets'] = array(
                'assets_number' => 'KND-' . date('Y-') . rand(pow(10, 4 - 1), pow(10, 4) - 1),
                'name' => $assets['name'],
                'category' => $assets['category'],
                'purchase_date' => $purchaseDate,
                'price' => $price,
                'accumulated_depreciation' => $accumulatedDepreciation,
                'book_value' => $bookValue,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s'),
            );

            $this->Assets->create();
            if ($this->Assets->save($dataSave)) {
                $this->Session->setFlash('Assets Successfuly Added');
                $this->redirect(array('action' => 'index'), null, true);
            } else {
                $this->Session->setFlash('Assets failed Added, Please Try Again..');
            }
        }
    }

    function view($id) {
        if (!$id) {
            $this->Session->setFlash('Assets invalid.');
            $this->redirect(array('action' => 'index'), null, true);
        } else {
            $assets = $this->Assets->findById($id)['Assets'];
            $this->set('assets', $assets);
        }
    }

    function edit($id) {
        if (!$id) {
            $this->Session->setFlash('Assets invalid.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        if (empty($this->data)) {
            $this->data = $this->Assets->findById($id);
        } else {
            $assets = $this->data['Assets'];
            $purchaseDate = $assets['purchase_date'];
            $purchaseDate = $purchaseDate['year'] . '-' . $purchaseDate['month'] . '-' . $purchaseDate['day'];
            $price = $assets['price'];

            $months = 0;
            if (strlen($purchaseDate) == 0) {
                $purchaseDate = NULL;
            } else {
                $nowDate = date('Y-m-d');
                $year1 = date('Y', strtotime($purchaseDate));
                $year2 = date('Y', strtotime($nowDate));

                $month1 = date('m', strtotime($purchaseDate));
                $month2 = date('m', strtotime($nowDate));

                $months = (($year2 - $year1) * 12) + ($month2 - $month1);
            }

            $accumulatedDepreciation = $months * $price / 48;
            $bookValue = $price - $accumulatedDepreciation;
            if ($bookValue < 0) $bookValue = 0;

            $data = $this->Assets->findById($id);
            $dataSave['Assets'] = array(
                'id' => $id,
                'assets_number' => $data['Assets']['assets_number'],
                'name' => $assets['name'],
                'category' => $assets['category'],
                'purchase_date' => $purchaseDate,
                'price' => $price,
                'accumulated_depreciation' => $accumulatedDepreciation,
                'book_value' => $bookValue,
                'created' => $data['Assets']['created'],
                'modified' => date('Y-m-d H:i:s'),
            );

            if ($this->Assets->save($dataSave)) {
                $this->Session->setFlash('Assets Successfuly Updated.');
                $this->redirect(array('action' => 'index'), null, true);
            } else {
                $this->Session->setFlash('Assets failed Updated, Please Try Again..');
            }
        }
    }

    function delete($id) {
        if (!$id) {
            $this->Session->setFlash('Assets invalid.');
            $this->redirect(array('action' => 'index'), null, true);
        }
        if ($this->Assets->delete($id)) {
            $this->Session->setFlash('Assets Successfuly Deleted.');
            $this->redirect(array('action' => 'index'), null, true);
        }
    }

}
