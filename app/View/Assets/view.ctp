<?php

echo $this->form->create('Assets');
?>

<fieldset>
    <legend>View Assets</legend>
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Asset Number</label>
                    <div class="col-sm-10"><?php echo $assets['assets_number']; ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10"><?php echo $assets['name']; ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Category</label>
                    <div class="col-sm-10"><?php echo $assets['category']; ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Purchase Date</label>
                    <div class="col-sm-10"><?php echo date('d-m-Y', strtotime($assets['purchase_date'])); ?></div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">Price</label>
                    <div class="col-sm-10"><?php echo number_format($assets['price'], 0, ",", "."); ?></div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
<?php echo $this->Html->link('List Assets', array('controller'=>'Assets', 'action'=>'index')); ?>