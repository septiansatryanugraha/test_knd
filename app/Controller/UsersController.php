<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    var $components = array('Session');

    public function login() {
//        $password = Security::hash('admin', null, true);
//        echo "<pre>";
//        print_r($password);
//        echo "</pre>";
//        echo "<br/>";

        $user = $this->Auth->user();
        $isLogin = 0;
        if (count($user) > 0) {
            $isLogin = 1;
        }
        if ($isLogin > 0) {
            return $this->redirect($this->Auth->redirect(array('controller' => 'Home', 'action' => 'index')));
        }
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                return $this->redirect($this->Auth->redirect(array('controller' => 'Home', 'action' => 'index')));
            }
            $this->Session->setFlash('Invalid username or password, try again');
        }
    }

    public function logout() {
        //return $this->redirect($this->Auth->logout());
        return $this->redirect($this->Auth->logout($this->Auth->redirect(array('controller' => 'Posts', 'action' => 'visitors'))));
    }

}
