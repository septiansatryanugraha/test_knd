<h1>Data Assets</h1>
<?php
echo $this->Form->create('', array('type' => 'get'));
echo $this->Form->input('q', array('label' => 'keyword'));
echo $this->Form->end('Search');
echo $this->Html->link($this->Form->button('Add Assets'), array('controller' => 'Assets', 'action' => 'add'), array('escape'=>false, 'title' => "Add Assets",));
?>

<table>
    <thead>
        <tr>
            <th><?php echo $this->Paginator->sort('assets_number','Assets Number'); ?></th>
            <th><?php echo $this->Paginator->sort('name','Name'); ?></th>
            <th><?php echo $this->Paginator->sort('category','Category'); ?></th>
            <th><?php echo $this->Paginator->sort('purchase_date','Purchase Date'); ?></th>
            <th><?php echo $this->Paginator->sort('price','Price'); ?></th>
            <th><?php echo $this->Paginator->sort('accumulated_depreciation','Accumulated Depreciation'); ?></th>
            <th><?php echo $this->Paginator->sort('book_value','BookValue'); ?></th>
            <th><?php echo $this->Paginator->sort('created','Created'); ?></th>
            <th><?php echo $this->Paginator->sort('modified','Modified'); ?></th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach ($assets as $key => $value) {?>
        <tr>
            <td><?php echo $value['Assets']['assets_number'] ?></td>
            <td><?php echo $value['Assets']['name'] ?></td>
            <td><?php echo $value['Assets']['category'] ?></td>
            <td><?php echo date("d-m-Y", strtotime($value['Assets']['purchase_date'])) ?></td>
            <td style="text-align: right;"><?php echo number_format($value['Assets']['price'], 0, ",", ".") ?></td>
            <td style="text-align: right;"><?php echo number_format($value['Assets']['accumulated_depreciation'], 0, ",", ".") ?></td>
            <td style="text-align: right;"><?php echo number_format($value['Assets']['book_value'], 0, ",", ".") ?></td>
            <td><?php echo date("d-m-Y H:i:s", strtotime($value['Assets']['created'])) ?></td>
            <td><?php echo date("d-m-Y H:i:s", strtotime($value['Assets']['modified'])) ?></td>
            <td>
                <?php echo $this->Html->link('View', array('action' => 'view', $value['Assets']['id'])) ?>
                <?php echo $this->Html->link('Edit', array('action' => 'edit', $value['Assets']['id'])) ?>
                <?php echo $this->Html->link('Delete', array('action' => 'delete', $value['Assets']['id']), array('confirm' => 'Are You Sure?')) ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<?php // debug($assets) ?>
<div class="paging">
    <?php
    echo $this->Paginator->prev(
        ' < Prev', array(), null, array('class' => 'prev disabled')
    );
    echo $this->Paginator->numbers(array('separator' => ''));
    echo $this->Paginator->next(
        'Next >', array(), null, array('class' => 'next disabled')
    );
    ?>
</div>